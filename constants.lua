-- atomic constants (change these!)
GRID_SIZE = 32
VIEWPORT_SIZE_X = 17
VIEWPORT_SIZE_Y = 11
VIEWPORT_X = 16 -- aesthetic placement valeu
VIEWPORT_Y = 16 -- aesthetic placement value
PLAYER_MOTION_SPEED = 10

-- compound constants (self-adjusting, don't change these!)