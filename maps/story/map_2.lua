return {
   { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
   { 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
   { 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1 },
   { 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1 },
   { 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1 },
   { 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
   { 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
   { 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1 },
   { 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
   { 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
   { 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
   { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
}, {
   { x = 11, y = 8, type = 4}
   }, { x = 2, y = 11 }, { x = 11 , y = 11 },
{
   { x = 2, y = 2, type = 1000 }, 
   { x = 8, y = 3, type = 1002 }
}, {
   { x = 2, y = 11,
     text = {
	"It is really dark here, why don't you look for a flashlight",
     }
   }, 
   { x = 2, y = 6,
     text = {
	"I think it is at the top here",
     }
   }, 
   { x = 2, y = 2,
     text = {
	"There it is, just 'grab' it",
	"Sadly there are no room for more items,",
	"maybe there is a backpack here?"
     }
   }, 
   { x = 8, y = 3,
     text = {
	"To 'grab' the backpack, you first need to 'drop'",
	"the flashlight"
     }
   }, 
   { x = 11, y = 7,
     text = {
	"Looks like it is a box south of you",
	"maybe you should try to 'push' it"
     }
   }
   }
