return {
   { 1, 1, 1, 1, 1, 1, 1, 1 },
   { 1, 1, 1, 0, 0, 0, 1, 1 },
   { 1, 3, 0, 0, 0, 0, 1, 1 },
   { 1, 1, 1, 0, 0, 3, 1, 1 },
   { 1, 3, 1, 1, 0, 0, 1, 1 },
   { 1, 0, 1, 0, 3, 0, 1, 1 },
   { 1, 0, 0, 3, 0, 0, 3, 1 },
   { 1, 0, 0, 0, 3, 0, 0, 1 },
   { 1, 1, 1, 1, 1, 1, 1, 1 }
}, { 
   { x = 5, y = 3, type = 2 },
   { x = 5, y = 4, type = 2 },
   { x = 5, y = 5, type = 2 },
   { x = 2, y = 7, type = 2 },
   { x = 4, y = 7, type = 2 },
   { x = 5, y = 7, type = 2 },
   { x = 6, y = 7, type = 2 },
   }, { x = 3, y = 3 }, { x = 2 , y = 8 },
{
   { x = 4, y = 3, type = 1003 },
   { x = 3, y = 8, type = 1004 },
   { x = 7, y = 8, type = 1005 },
   { x = 6, y = 2, type = 1006 }
}, {
   { x = 2, y = 11,
     text = {
	"Welcome to the Sokoban level",
	"This is a rather tricky level with a lot of boxes",
	"you need to move each box to a switch to open the gate"
     }
   }
   }
