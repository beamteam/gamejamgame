return {
   { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
   { 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
   { 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1 },
   { 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1 },
   { 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1 },
   { 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
   { 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
   { 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
   { 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
   { 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1 },
   { 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
   { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
}, { }, { x = 6, y = 7 }, { x = 2, y = 3 }, 
{ 
}, {
   { x = 6, y = 7,
     text = { 
	"It looks like you are lost, why not try to 'look' around",
	"You can 'look' in all the directions",
	"If you are stuck try to move 'up' or 'down'" }
   }, 
   { x = 2, y = 2,
     text = {
	"You need to 'escape' to enter the goal",
	"It is also important to 'turn' in the right direction"
     }
   }
   }
