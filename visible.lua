--[[
   ABSTRACT CLASS
   a 'visible thing' has a visibility boolean
]]
Visible = Class{}

function Visible:init()
   self.visible = false
end
